import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

DEBUG = False

SECRET_KEY = os.environ.get('SECRET_KEY')

PREREQUISITE_APPS = ['corsheaders']

PROJECT_APPS = []

INSTALLED_APPS = PREREQUISITE_APPS + PROJECT_APPS

MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
]

ROOT_URLCONF = 'code_runner.urls'

WSGI_APPLICATION = 'code_runner.wsgi.application'
