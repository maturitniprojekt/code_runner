from django.urls import path, include

urlpatterns = [
    path('builder/', include('builder.urls')),
    path('executor/', include('executor.urls'))
]
