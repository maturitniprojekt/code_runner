FROM alpine:3.8
MAINTAINER Lubos Zapotocny <zapotocnylubos@gmail.com>

# Issues:
# * Added musl package because incompatibility in apline:3.8
#     https://github.com/frol/docker-alpine-mono/issues/9

ENV DEPENDENCIES_PATH=/executor/dependencies \
    WORSKPACE_PATH=/executor/workspace

ENV MUSL_VERSION=\>1.1.20 \
    MONO_VERSION=~5.10 \
    NUNIT_VERSION=3.11.0 \
    CONSOLE_RUNNER_VERSION=3.9.0

RUN apk add --no-cache musl=$MUSL_VERSION --repository http://dl-cdn.alpinelinux.org/alpine/edge/main && \
    apk add --no-cache mono=$MONO_VERSION --repository http://dl-cdn.alpinelinux.org/alpine/edge/testing && \
    apk add --no-cache --virtual=.build-dependencies ca-certificates && \
    cert-sync /etc/ssl/certs/ca-certificates.crt && \
    apk del .build-dependencies

WORKDIR $DEPENDENCIES_PATH

RUN wget https://dist.nuget.org/win-x86-commandline/latest/nuget.exe && \
    mono nuget.exe install NUnit -Version $NUNIT_VERSION -ExcludeVersion && \
    mono nuget.exe install NUnit.ConsoleRunner -Version $CONSOLE_RUNNER_VERSION -ExcludeVersion

ENV MONO_PATH=$DEPENDENCIES_PATH/NUnit.ConsoleRunner/tools:$DEPENDENCIES_PATH/NUnit/lib/net45

WORKDIR $WORSKPACE_PATH