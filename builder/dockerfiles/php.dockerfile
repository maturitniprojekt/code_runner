FROM alpine:3.8
MAINTAINER Lubos Zapotocny <zapotocnylubos@gmail.com>

ENV DEPENDENCIES_PATH=/executor/dependencies
ENV WORSKPACE_PATH=/executor/workspace

RUN apk add --no-cache php7 && \
    apk add --no-cache php7-phar && \
    apk add --no-cache php7-dom

WORKDIR $DEPENDENCIES_PATH

RUN wget https://phar.phpunit.de/phpunit-7.phar -O phpunit && \
    chmod +x phpunit && \
    mv phpunit /usr/local/bin/phpunit

WORKDIR $WORSKPACE_PATH