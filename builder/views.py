import os.path

import docker
from django.http import JsonResponse
from django.views.decorators.http import require_POST
from docker.errors import BuildError


@require_POST
def build(request):
    client = docker.from_env()
    dockerfile_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), 'dockerfiles'))

    try:
        # get compiler name from url
        lang = request.POST['lang']

        try:
            output, generator = client.images.build(
                path=dockerfile_dir,
                dockerfile=f'{lang}.dockerfile',
                tag=lang,
                nocache=True
            )

            output = [line.get('stream') for line in generator]

            return JsonResponse({'data': output})
        except BuildError:
            return JsonResponse(status=422, data={'error': 'BUILD_FAILED'})
    except KeyError:
        return JsonResponse(status=422, data={'error': 'MISSING_PARAMETERS'})
