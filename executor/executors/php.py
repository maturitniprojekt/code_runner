from executor.executors.base import BaseExecutor
from executor.utils import ExecutionOutput


class PHPExecutor(BaseExecutor):
    def __init__(self):
        super().__init__(image_name='php', timeout_time=10)

    def execute_code(self, code):
        pass

    def execute_tests(self, code: str, tests: str) -> ExecutionOutput:
        files = {
            'autoload.php': "<?php require 'code.php';",
            'code.php': code,
            'codeTest.php': tests,  # must end with Test.php
        }

        command = " ".join([
            "phpunit",
            "--bootstrap autoload.php",
            ".",
            "--log-junit",
            "TestResult.xml"
        ])

        return self.execute(
            command=command,
            files=files
        )
