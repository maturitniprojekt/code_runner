from executor.executors.base import BaseExecutor
from executor.utils import ExecutionOutput


class CsharpExecutor(BaseExecutor):

    def __init__(self):
        super().__init__(image_name='csharp', timeout_time=10)

    def execute_code(self, code):
        pass

    def execute_tests(self, code: str, tests: str) -> ExecutionOutput:
        files = {
            'code.cs': code,
            'tests.cs': tests,
        }

        compile_command = " ".join([
            "mcs",
            "code.cs",
            "tests.cs",
            "-target:library",
            "-out:onlytests.dll",
            "-r:/executor/dependencies/NUnit/lib/net45/nunit.framework.dll",
        ])

        run_tests_command = " ".join([
            "mono",
            "/executor/dependencies/NUnit.ConsoleRunner/tools/nunit3-console.exe onlytests.dll",
        ])

        command = f"sh -c \"{compile_command} && {run_tests_command}\""

        return self.execute(
            command=command,
            files=files
        )
