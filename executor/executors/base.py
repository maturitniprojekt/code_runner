import os.path
from tempfile import TemporaryDirectory
from typing import Dict

import docker
from requests.exceptions import ConnectionError

from executor.utils import ExecutionOutput


class BaseExecutor:
    dependencies_path: str = "/executor/dependencies"
    workspace_path: str = "/executor/workspace"

    image_name: str
    timeout_time: float

    def __init__(self, image_name: str, timeout_time: float):
        self.client = docker.from_env()

        self.image_name = image_name
        self.timeout_time = timeout_time

    def prepare_files(self, dir_path: str, files: Dict[str, str]) -> None:
        for file_name, file_content in files.items():
            open(os.path.join(dir_path, file_name), "w").write(file_content)

    def prepare_shared_volume(self, dir_path: str) -> dict:
        return {
            dir_path: {
                'bind': self.workspace_path,
                'mode': 'rw'
            }
        }

    def execute(self, command: str, files: Dict[str, str]):
        with TemporaryDirectory(dir='/tmp') as dir_path:

            self.prepare_files(dir_path=dir_path, files=files)
            volumes = self.prepare_shared_volume(dir_path=dir_path)

            output = ExecutionOutput()

            try:
                container = self.client.containers.create(
                    image=self.image_name,
                    command=command,
                    volumes=volumes
                )

                container.start()
                try:
                    container.wait(timeout=self.timeout_time)  # TODO dynamic time
                except ConnectionError:
                    output.error = 'TIMEOUT_ERROR'
                    container.kill()

                output.stderr = container.logs(stdout=False, stderr=True)
                output.stdout = container.logs(stdout=True, stderr=False)

                container.remove()

            except ConnectionError:
                output.error = 'EXECUTOR_ERROR'  # TODO log this

            output.load_execution_result(os.path.join(dir_path, 'TestResult.xml'))  # TODO dynamic filename

            return output

    # def execute(self, host_folder: str, command: str) -> ExecutionOutput:
    #     volumes = {
    #         host_folder: {
    #             'bind': self.workspace_path,
    #             'mode': 'rw'
    #         }
    #     }
    #
    #     result = ExecutionOutput()
    #
    #     try:
    #         container = self.client.containers.create(image=self.image_name, command=command, volumes=volumes)
    #
    #         container.start()
    #         try:
    #             container.wait(timeout=5)  # TODO not fixed time
    #         except ConnectionError:
    #             result.error = 'TIMEOUT_ERROR'
    #             container.kill()
    #
    #         result.stderr = container.logs(stdout=False, stderr=True)
    #         result.stdout = container.logs(stdout=True, stderr=False)
    #
    #         container.remove()
    #
    #         return result
    #     except ConnectionError:
    #         result.error = 'EXECUTOR_ERROR'  # TODO log this
    #     finally:
    #         return result
