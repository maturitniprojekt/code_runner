from django.http import JsonResponse
from django.views.decorators.http import require_POST

from executor.converters.csharp import CsharpConverter
from executor.converters.php import PHPConverter
from executor.executors.csharp import CsharpExecutor
from executor.executors.php import PHPExecutor

runners = {
    'csharp': CsharpExecutor,
    'php': PHPExecutor,
}

converters = {
    'csharp': CsharpConverter,
    'php': PHPConverter
}


@require_POST
def execute_tests(request):
    try:
        # get compiler name from url
        lang = request.POST['lang']
        # get code from body
        code = request.POST['code']
        # get tests from body
        tests = request.POST['tests']

        runner = runners[lang]()
        output = runner.execute_tests(code, tests)

        converter = converters[lang]()
        try:
            if output.result.raw_result:
                output.result.converted_result = converter.convert(output.result.raw_result)
        except ValueError:
            pass

        return JsonResponse(data={'output': output.to_json()})
    except KeyError as e:
        return JsonResponse(status=422, data={'error': 'MISSING_PARAMETERS'})
