from executor.converters.base import BaseConverter


class PHPConverter(BaseConverter):
    def convert(self, data: str) -> dict:
        xml_dict = self.parse_to_dict(data)

        root = xml_dict.get('testsuites')

        if root is None:
            raise ValueError('invalid xml data')

        root_suite = root.get('testsuite')

        if root_suite is None:
            raise ValueError('invalid xml data')

        return self.convert_test_suite(root_suite)

    def convert_test_suite(self, test_suite):
        child_suites = self.ensure_list(test_suite.get('testsuite')) if 'testsuite' in test_suite else []
        child_cases = self.ensure_list(test_suite.get('testcase')) if 'testcase' in test_suite else []

        return {
            'name': test_suite.get('@name'),
            'total': int(test_suite.get('@tests')),
            'failures': int(test_suite.get('@failures')),
            'duration': float(test_suite.get('@time')),
            'testSuites': [self.convert_test_suite(suite) for suite in child_suites],
            'testCases': [self.convert_test_case(case) for case in child_cases]
        }

    def convert_test_case(self, test_case):
        return {
            'name': test_case.get('@name'),
            'duration': test_case.get('@time'),
            'failure': test_case.get('failure').get('#text') if 'failure' in test_case else None,
            'output': test_case.get('system-out') if 'system-out' in test_case else None
        }
