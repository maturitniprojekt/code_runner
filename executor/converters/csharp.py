from executor.converters.base import BaseConverter


class CsharpConverter(BaseConverter):
    def convert(self, data: str) -> dict:
        xml_dict = self.parse_to_dict(data)

        root = xml_dict.get('test-run')

        if root is None:
            raise ValueError('invalid xml data')

        root_suite = root.get('test-suite')

        if root_suite is None:
            raise ValueError('invalid xml data')

        return self.convert_test_suite(root_suite)

    def convert_test_suite(self, test_suite):
        child_suites = self.ensure_list(test_suite.get('test-suite')) if 'test-suite' in test_suite else []
        child_cases = self.ensure_list(test_suite.get('test-case')) if 'test-case' in test_suite else []

        return {
            'name': test_suite.get('@name'),
            'total': int(test_suite.get('@total')),
            'failures': int(test_suite.get('@failed')),
            'duration': float(test_suite.get('@duration')),
            'testSuites': [self.convert_test_suite(suite) for suite in child_suites],
            'testCases': [self.convert_test_case(case) for case in child_cases]
        }

    def convert_test_case(self, test_case):
        return {
            'name': test_case.get('@name'),
            'duration': test_case.get('@duration'),
            'failure': test_case.get('failure').get('message') if 'failure' in test_case else None,
            'output': test_case.get('output') if 'output' in test_case else None
        }
