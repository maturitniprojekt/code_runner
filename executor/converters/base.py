import xmltodict
from xml.parsers.expat import ExpatError


class BaseConverter:
    @staticmethod
    def parse_to_dict(data: str) -> dict:
        try:
            return xmltodict.parse(data)
        except ExpatError:
            raise ValueError("data must be in xml format")

    @staticmethod
    def ensure_list(obj) -> list:
        return [obj] if isinstance(obj, dict) else obj

    def convert(self, data: str) -> dict:
        raise NotImplementedError
