from django.urls import path

from . import views

urlpatterns = [
    path('execute-tests', views.execute_tests)
]
