class ExecutionResult:
    raw_result: str = None
    converted_result: str = None

    def to_json(self):
        return {
            'rawResult': self.raw_result,
            'convertedResult': self.converted_result
        }


class ExecutionOutput:
    error: str = None
    stderr: bytes = None
    stdout: bytes = None
    result: ExecutionResult

    def __init__(self):
        self.result = ExecutionResult()

    def load_execution_result(self, file_path):
        try:
            self.result.raw_result = open(file_path).read()
        except FileNotFoundError:
            pass

    def get_stderr(self):
        return self.stderr.decode('utf-8')

    def get_stdout(self):
        return self.stdout.decode('utf-8')

    def to_json(self):
        return {
            'error': self.error,
            'stderr': self.get_stderr(),
            'stdout': self.get_stdout(),
            'result': self.result.to_json()
        }
